Hello World from Archaea
========================

This is a simple "Hello, World!" program which will be used as a skeleton for
other projects as well as a simple test case for various bits of
infrastructure.
